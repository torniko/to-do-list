﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToDoList
{
    public class ToDo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DayOfWeek Weekday { get; set; }
        public DateTime DeadLine { get; set; }
        public bool IsDone { get; set; }




    }
}
