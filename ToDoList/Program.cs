﻿using System;

namespace ToDoList
{
    class Program
    {
        static void Main(string[] args)
        {
            var todo = new MockToDo();

            // სახელის მიხედვით აკეთებს ტასკის იდენტიფიკაციას,
            // რომელსაც isdone false-დან trued - უნდა გადაუკეთოს.
            todo.ChangeIsDone("seirnoba");


            todo.AddTask("filmisnaxva", DayOfWeek.Saturday,  new DateTime(10, 00), false );

            todo.ChangeIsDone("filmisnaxva");
            foreach (var item in todo.ToDoList)
            {
                Console.Write(item.Name +" ");
                Console.WriteLine(item.IsDone);
            }

        }
    }
}
