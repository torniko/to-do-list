﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToDoList
{
    public class MockToDo
    {
        public List<ToDo> ToDoList = new List<ToDo>() {
            { new ToDo {  Name = "varjishi", Weekday = DayOfWeek.Monday, DeadLine = new DateTime(10,00), IsDone = false } },
            { new ToDo {  Name = "varjishi", Weekday = DayOfWeek.Tuesday, DeadLine = new DateTime(10,00), IsDone = false } },
            { new ToDo {  Name = "seirnoba", Weekday = DayOfWeek.Wednesday, DeadLine = new DateTime(10,00), IsDone = false } },
            { new ToDo {  Name = "varjishi", Weekday = DayOfWeek.Friday, DeadLine = new DateTime(10,00), IsDone = false } },
        };

        public void AddTask(string name, DayOfWeek weekday, DateTime datetime, bool isdone)
        {
            ToDoList.Add(new ToDo { Name = name, Weekday = weekday, DeadLine = datetime, IsDone = isdone }); 
        }

        public void ChangeIsDone(string namez)
        {
            foreach (var item in ToDoList)
            {
                if(item.Name == namez)
                {
                    item.IsDone = true;
                }
            }
        }

    }
}
